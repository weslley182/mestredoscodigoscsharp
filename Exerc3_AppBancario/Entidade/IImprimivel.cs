﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exerc3_AppBancario.Entidade
{
    public interface IImprimivel
    {
        public string MostrarDados();
    }
}
