﻿using Exerc8_ListaOrdem.Servico;
using System;
using System.Linq;

namespace Exerc8_ListaOrdem
{
    class Program
    {
        static void Main(string[] args)
        {
            ListaService lista = new ListaService();
            lista.ExecutarLista();
        }
    }
}
